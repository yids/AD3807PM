//*********************************************************************************
//**
//** Project.........: AD8307 based RF Power Meter
//**
//** Copyright (C) 2013  Loftur E. Jonasson  (tf3lj [at] arrl [dot] net)
//**
//** This program is free software: you can redistribute it and/or modify
//** it under the terms of the GNU General Public License as published by
//** the Free Software Foundation, either version 3 of the License, or
//** (at your option) any later version.
//**
//** This program is distributed in the hope that it will be useful,
//** but WITHOUT ANY WARRANTY; without even the implied warranty of
//** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//** GNU General Public License for more details.
//**
//** You should have received a copy of the GNU General Public License
//** along with this program.  If not, see <http://www.gnu.org/licenses/>.
//**
//** Platform........: AT90usb1286 @ 16MHz
//**
//** Initial version.: 2012-04-01, Loftur Jonasson, TF3LJ
//**
//**
//** History.........: Check the PM.c file
//**
//*********************************************************************************

#include 	<math.h>
#include	<stdlib.h>
#include	"AD8307PM.h"

//
//-----------------------------------------------------------------------------
//			Display Power in dBm
//-----------------------------------------------------------------------------
//
void lcd_display_power_db(void)
{
	static uint8_t mode_timer = 0;

	lcdClear();

	//------------------------------------------
	// Display mode intro for a time
	if(Status & MODE_DISPLAY)
	{
		if(Status & MODE_CHANGE)
		{
			Status &= ~MODE_CHANGE;			// Clear display change mode
			mode_timer = 0;					// New mode, reset timer
		}
		lcdGotoXY(0,0);
		lcdPrintData("Display Power in",16);
		lcdGotoXY(0,1);
		lcdPrintData("dB milliWatts ->",16);
		mode_timer++;
		if (mode_timer == MODE_INTRO_TIME)	// MODE_INTRO_TIME in tenths of seconds
		{
			mode_timer = 0;
			Status &= ~MODE_DISPLAY;		// Clear display change mode
		}
	}
	else
	{
		//------------------------------------------
		// Power indication, dBm average
		lcdGotoXY(0,0);
		print_dbm((int16_t) (power_db_avg*10.0));
		lcdPrintData(lcd_buf,strlen(lcd_buf));
		lcdPrintData(" avg",4);

		//------------------------------------------
		// Power indication, dBm pep
		lcdGotoXY(0,1);
		print_dbm((int16_t) (power_db_pep*10.0));
		lcdPrintData(lcd_buf,strlen(lcd_buf));
		lcdPrintData(" pep",4);

		//------------------------------------------
		// Amplitude Modulation Index
		// Only display if at an useful level or above
		if (power_db_pep >= R.AM_threshold)
		{
			lcdGotoXY(14,0);
			lcdPrintData("AM",2);
			lcdGotoXY(12,1);
			sprintf(lcd_buf,"%3d%%",modulation_index);
			lcdPrintData(lcd_buf,strlen(lcd_buf));			
		}
	}
}


//
//-----------------------------------------------------------------------------
//			Display Power in Watts
//-----------------------------------------------------------------------------
//
void lcd_display_power_w(void)
{
	static uint8_t mode_timer = 0;

	lcdClear();

	//------------------------------------------
	// Display mode intro for a time
	if(Status & MODE_DISPLAY)
	{
		if(Status & MODE_CHANGE)
		{
			Status &= ~MODE_CHANGE;			// Clear display change mode
			mode_timer = 0;					// New mode, reset timer
		}
		lcdGotoXY(0,0);
		lcdPrintData("Display Power in",16);
		lcdGotoXY(0,1);
		lcdPrintData("Watts         ->",16);
		mode_timer++;
		if (mode_timer == MODE_INTRO_TIME)	// MODE_INTRO_TIME in tenths of seconds
		{
			mode_timer = 0;
			Status &= ~MODE_DISPLAY;		// Clear display change mode
		}
	}
	else
	{
		//------------------------------------------
		// Power indication, average
		lcdGotoXY(0,0);
		// Wattage Printout)
		print_p_mw(power_mw_avg);
		lcdPrintData(lcd_buf,strlen(lcd_buf));
		lcdPrintData(" avg",4);

		//------------------------------------------
		// Power indication, 1 second PEP
		lcdGotoXY(0,1);
		// Wattage Printout)
		print_p_mw(power_mw_pep);
		lcdPrintData(lcd_buf,strlen(lcd_buf));
		lcdPrintData(" pep",4);

		//------------------------------------------
		// Amplitude Modulation Index
		// Only display if at an useful level or above
		if (power_db_pep >= R.AM_threshold)
		{
			lcdGotoXY(14,0);
			lcdPrintData("AM",2);
			lcdGotoXY(12,1);
			sprintf(lcd_buf,"%3d%%",modulation_index);
			lcdPrintData(lcd_buf,strlen(lcd_buf));
		}
	}
}

//
//-----------------------------------------------------------------------------
//			Display Voltage (RMS and Peak-to-Peak) in 50 ohm
//-----------------------------------------------------------------------------
//
void lcd_display_voltage(void)
{
	double 	voltage;
	static uint8_t mode_timer = 0;

	lcdClear();

	//------------------------------------------
	// Display mode intro for a time
	if(Status & MODE_DISPLAY)
	{
		if(Status & MODE_CHANGE)
		{
			Status &= ~MODE_CHANGE;			// Clear display change mode
			mode_timer = 0;					// New mode, reset timer
		}
		lcdGotoXY(0,0);
		lcdPrintData("Display average",15);
		lcdGotoXY(0,1);
		lcdPrintData("Voltage (50ohm)>",16);
		mode_timer++;
		if (mode_timer == MODE_INTRO_TIME)	// MODE_INTRO_TIME in tenths of seconds
		{
			mode_timer = 0;
			Status &= ~MODE_DISPLAY;		// Clear display change mode
		}
	}
	else
	{
		//------------------------------------------
		// Voltage (in 50 ohms) indication, RMS
		lcdGotoXY(0,0);
		voltage = sqrt(power_mw_avg*50/1000.0);
		print_v(voltage);
		lcdPrintData(lcd_buf,strlen(lcd_buf));
		lcdPrintData(" rms",4);

		//------------------------------------------
		// Voltage (in 50 ohms) indication, Vpeak-to-peak
		lcdGotoXY(0,1);
		voltage = sqrt(power_mw*50/1000.0) * 2 * sqrt(2);
		print_v(voltage);
		lcdPrintData(lcd_buf,strlen(lcd_buf));
		lcdPrintData(" p-to-p",7);
	}
}


//
//-----------------------------------------------------------------------------
//			Display Bargraph meter
//-----------------------------------------------------------------------------
//
void lcd_display_bargraph_full(void)
{
	int16_t	db10;
	static uint8_t mode_timer = 0;

	db10 = (int16_t) (power_db*10.0);
	
	lcdClear();

	//------------------------------------------
	// Display mode intro for a time
	if(Status & MODE_DISPLAY)
	{
		if(Status & MODE_CHANGE)
		{
			Status &= ~MODE_CHANGE;			// Clear display change mode
			mode_timer = 0;					// New mode, reset timer
		}
		lcdGotoXY(0,0);
		lcdPrintData("Bargraph Meter->",16);
		lcdGotoXY(0,1);
		lcdPrintData("(average power)",15);
		mode_timer++;
		if (mode_timer == MODE_INTRO_TIME)	// MODE_INTRO_TIME in tenths of seconds
		{
			mode_timer = 0;
			Status &= ~MODE_DISPLAY;		// Clear display change mode
		}
	}
	else
	{
		//------------------------------------------
		// Power indication, dBm average
		lcdGotoXY(0,0);
		print_dbm(db10);
		lcdPrintData(lcd_buf,strlen(lcd_buf));
	
		//------------------------------------------
		// Power indication, average
		lcdGotoXY(9,0);
		print_p_mw(power_mw_avg);
		lcdPrintData(lcd_buf,strlen(lcd_buf));

		//------------------------------------------
		// Bargraph
		lcdGotoXY(0,1);
		lcdProgressBarPeak((db10 + BAR_MIN_VALUE), 0, BAR_FULL_RANGE, BAR_LENGTH);
	}
}


//
//-----------------------------------------------------------------------------
//			Display Bargraph meter, +/- 8dB fullscale
//-----------------------------------------------------------------------------
//
void lcd_display_bargraph_16db(void)
{
	int16_t	db10;
	int16_t bar_input;
	static uint8_t mode_timer = 0;

	db10 = (int16_t) (power_db*10.0);
	
	lcdClear();

	//------------------------------------------
	// Display mode intro for a time
	if(Status & MODE_DISPLAY)
	{
		if(Status & MODE_CHANGE)
		{
			Status &= ~MODE_CHANGE;			// Clear display change mode
			mode_timer = 0;					// New mode, reset timer
		}
		lcdGotoXY(0,0);
		lcdPrintData("+/- 8dB Bargraph",16);
		lcdGotoXY(0,1);
		lcdPrintData("Push to Center->",16);
		mode_timer++;
		if (mode_timer == MODE_INTRO_TIME)	// MODE_INTRO_TIME in tenths of seconds
		{
			mode_timer = 0;
			Status &= ~MODE_DISPLAY;		// Clear display change mode
		}
	}
	else
	{
		//------------------------------------------
		// Power indication, dBm average
		lcdGotoXY(0,0);
		print_dbm(db10);
		lcdPrintData(lcd_buf,strlen(lcd_buf));
	
		//------------------------------------------
		// Power indication, average
		lcdGotoXY(9,0);
		print_p_mw(power_mw_avg);
		lcdPrintData(lcd_buf,strlen(lcd_buf));

		//------------------------------------------
		// Bargraph +/-8dB fullscale
		bar_input = db10 - power_snapshot_db10 + BAR_FINE_RES/2;	// Calculate bar length
		if (bar_input < 0) bar_input = 0;							// Set bounds
		if (bar_input > BAR_FINE_RES) bar_input = BAR_FINE_RES;

		lcdGotoXY(0,1);												// Display the bargraph
		lcdProgressBarPeak(bar_input, 0, BAR_FINE_RES, BAR_LENGTH);
	}
}


//
//-----------------------------------------------------------------------------
//			Display a Mixed bag of irrelevant stuff
//-----------------------------------------------------------------------------
//
void lcd_display_mixed(void)
{
	double 	output_voltage;
	uint16_t power, power_sub;
	
	lcdClear();

	lcdGotoXY(0,0);
	lcdPrintData("Dbg:",4);

	//------------------------------------------
	// AD8307 voltage indication
	lcdGotoXY(4,0);
	output_voltage = ad8307_ad * 4 / 1000.0;
	power_sub = output_voltage * 1000;
	power = power_sub / 1000;
	power_sub = power_sub % 1000;
	sprintf(lcd_buf,"%u.%03uV %4u", power, power_sub, ad8307_ad);	
	lcdPrintData(lcd_buf,strlen(lcd_buf));
	//------------------------------------------
	// Calibrate 1
	lcdGotoXY(0,1);
	sprintf (lcd_buf,"%3d",R.calibrate[0].db10m);
	lcdPrintData(lcd_buf,strlen(lcd_buf));
	sprintf (lcd_buf,"%4u",R.calibrate[0].ad);
	lcdPrintData(lcd_buf,strlen(lcd_buf));
	//------------------------------------------
	// Calibrate 2
	lcdGotoXY(8,1);
	sprintf (lcd_buf,"%4d",R.calibrate[1].db10m);
	lcdPrintData(lcd_buf,strlen(lcd_buf));
	sprintf (lcd_buf,"%4u",R.calibrate[1].ad);
	lcdPrintData(lcd_buf,strlen(lcd_buf));
}

