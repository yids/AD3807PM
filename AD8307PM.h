//*********************************************************************************
//**
//** Project.........: AD8307 based RF Power Meter
//**
//** Copyright (C) 2013  Loftur E. Jonasson  (tf3lj [at] arrl [dot] net)
//**
//** This program is free software: you can redistribute it and/or modify
//** it under the terms of the GNU General Public License as published by
//** the Free Software Foundation, either version 3 of the License, or
//** (at your option) any later version.
//**
//** This program is distributed in the hope that it will be useful,
//** but WITHOUT ANY WARRANTY; without even the implied warranty of
//** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//** GNU General Public License for more details.
//**
//** You should have received a copy of the GNU General Public License
//** along with this program.  If not, see <http://www.gnu.org/licenses/>.
//**
//** Platform........: AT90usb1286 @ 16MHz
//**
//** Initial version.: 2012-04-01, Loftur Jonasson, TF3LJ / VE2LJX
//**
#define                VERSION "1.08"
#define                DATE    "2014-02-13"
//**
//** History.........: Check the PM.c file
//**
//*********************************************************************************

#ifndef _TF3LJ_PM_H_
#define _TF3LJ_PM_H_ 1

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/power.h>
#include <lcd.h>

#ifdef WITH_USB	
#include "usb_serial.h"	
#endif

//
//-----------------------------------------------------------------------------
// Features Selection
//-----------------------------------------------------------------------------
//
  
//-----------------------------------------------------------------------------
// EEPROM settings Serial Number. Increment this number when firmware mods necessitate
// fresh "Factory Default Settings" to be forced into the EEPROM at first boot after
// an upgrade
#define COLDSTART_REF		0x06// When started, the firmware examines this "Serial Number
								// and enforce factory reset if there is a mismatch.
								// This is useful if the EEPROM structure has been modified


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Various Initial Default settings for Meter
// (many of these are configurable by user through Config Menu or USB commands)
//
//-----------------------------------------------------------------------------
// PEP envelope sampling time in 5ms increments (200 = 1 second)
#define PEP_PERIOD			 200		// 200, 500 or 1000, (1s, 2.5 or 5s)
//-----------------------------------------------------------------------------
// Threshold in dBm to display Amplitude Modulation
#define AM_THRESHOLD		 -60		// -80 to 20 dBm.  A value of 20 dBm effectively turns it off.
//-----------------------------------------------------------------------------
// Mode Intro Time (decides for how long mode intro is displayed when turning encoder
#define MODE_INTRO_TIME		  10		// Tenths of a second (10 equals 1s)
//-----------------------------------------------------------------------------
//USB Serial data out on or off
#define USB_DATA			FALSE		// Default is USB Serial Port is disable
//#define USB_DATA			TRUE		// Default is USB Serial Port is enabled
//-----------------------------------------------------------------------------
//Defs for default Gain Settings (dB *10)
#define	GAINSET1			-400		// 40 dB Attenuator
#define	GAINSET2			-700		// 15 dB Attenuator
#define	GAINSET3			 180		// 18.0 dB Gain (amplifier)
//-----------------------------------------------------------------------------
//Defs for AD8307 default Calibration (dBm *10)
#define	CAL1_NOR_VALUE		   0		// Default for AD8717 at   0 dBm
#define	CAL1_RAW_DEFAULT	 539		// Default for AD8717 at   0 dBm
#define	CAL2_NOR_VALUE		-400		// Default for AD8717 at -40 dBm
#define	CAL2_RAW_DEFAULT	 289		// Default for AD8717 at -40 dBm

//-----------------------------------------------------------------------------
// LED Blink
//
// None, or only one of the four should be selected
#define FAST_LOOP_THRU_LED	0	// Blink PB2 LED every time, when going through the mainloop *OR*
#define	MS_LOOP_THRU_LED	1	// Blink PB2 LED every 1ms, when going through the mainloop *OR*
#define	MED_LOOP_THRU_LED	0	// Blink PB2 LED every 10ms, when going through the mainloop *OR*
#define SLOW_LOOP_THRU_LED	0	// Blink PB2 LED every 100ms, when going through the mainloop


//-----------------------------------------------------------------------------
//Defs for the ADC Voltage Reference
// can be ADC_REF_POWER,  ADC_REF_INTERNAL (2.56V) or ADC_REF_EXTERNAL
#define ADC_METER_REF	ADC_REF_EXTERNAL
//
// The below normally not touched.  Used for one shot calibration
//
#if ADC_METER_REF == ADC_REF_EXTERNAL
#define VREF				4.096			// External Reference at 4.096V
#elif ADC_METER_REF == ADC_REF_INTERNAL
#define VREF				2.56			// The internal AT90USB reference
#else
#define VREF				5.0				// 5V (ADC_REF_POWER)
#endif
#define REF_SLOPE		(1024/VREF*0.0025)	// AD8307 slope is 2.5mV per 0.1dB
											// REF_SLOPE is used for One Level Calibration

//-----------------------------------------------------------------------------
//DEFS for LED
#define LED_PORT			PORTB		// port for the LED
#define LED_DDR				DDRB		// port for the LED
#define LED					(1<<6)		// pin for LED


//-----------------------------------------------------------------------------
//DEFS for LCD Display are in avrlib/lcdconf.h

//-----------------------------------------------------------------------------
// DEFS for the Rotary Encoder VFO function
// Configuration of the two input pins, Phase A and Phase B
// They can be set up to use any pin on two separate input ports
#define ENC_A_PORT		PORTD				// PhaseA port register
#define ENC_A_DDR		DDRD				// PhaseA port direction register
#define ENC_A_PORTIN	PIND				// PhaseA port input register
#define ENC_A_PIN		(1 << 1)			// PhaseA port pin
#define ENC_B_PORT		PORTD				// PhaseB port register
#define ENC_B_DDR		DDRD				// PhaseB port direction register
#define ENC_B_PORTIN	PIND				// PhaseB port input register
#define ENC_B_PIN		(1 << 2)			// PhaseB port pin

#define	ENC_RES_DIVIDE		1				// Default reduction of the Encoder Resolution
#define ENCODER_DIR_REVERSE	0				// Reverse the direction of the Rotary Encoder

//-----------------------------------------------------------------------------
// Definitions for the Pushbutton Encoder functionality
#define ENC_PUSHB_PORT		PORTD
#define ENC_PUSHB_DDR		DDRD
#define	ENC_PUSHB_INPORT	PIND
#define	ENC_PUSHB_PIN		(1 << 3)		// PD4

#define ENC_PUSHB_MIN		1				// Min pushdown for valid push (x 10ms)
#define	ENC_PUSHB_MAX		10				// Min pushdown for memory save (x 10ms)

//-----------------------------------------------------------------------------
// Bargraph bounds
#define BAR_MIN_VALUE		800	// -80dB
#define BAR_FULL_RANGE		1000// 100dB full range
#define	BAR_FINE_RES		160	// 16dB for full scale variation if in fine resolution mode
#define BAR_LENGTH			16	// 16 characters long

//-----------------------------------------------------------------------------
// Select Bargraph display style
#define BARGRAPH_STYLE_1	1	// Used if LCD bargraph alternatives.  N8LP LP-100 look alike bargraph         *OR*
#define BARGRAPH_STYLE_2	0	// Used if LCD bargraph alternatives.  Bargraph with level indicators          *OR*
#define BARGRAPH_STYLE_3	0	// Used if LCD bargraph alternatives.  Another bargraph with level indicators  *OR*
#define BARGRAPH_STYLE_4	0	// Used if LCD bargraph alternatives.  Original bargraph, Empty space enframed *OR*
#define BARGRAPH_STYLE_5	0	// Used if LCD bargraph alternatives.  True bargraph, Empty space is empty


//
//-----------------------------------------------------------------------------
// Miscellaneous software defines, functions and variables
//-----------------------------------------------------------------------------
//

//-----------------------------------------------------------------------------
// Flags

// DEFS for all kinds of Flags
extern uint8_t			Status;
#define ENC_CHANGE		(1 << 0)			// Indicates that Encoder value has been modified
#define SHORT_PUSH		(1 << 1)			// Short Push Button Action
#define	LONG_PUSH		(1 << 2)			// Long Push Button Action
#define BARGRAPH_CAL	(1 << 3)			// 16dB Bargraph has been re-centred
#define MODE_CHANGE		(1 << 4)			// Display mode has changed
#define MODE_DISPLAY	(1 << 5)			// Display mode has changed
#define USB_AVAILABLE	(1 << 6)			// USB Serial Data Output Enabled

// Operation Mode Flags
extern	uint8_t			Menu_Mode;			// Which Menu Mode is active
#define POWER_DB		(1 << 0)
#define	POWER_W			(1 << 1)
#define VOLTAGE			(1 << 2)
#define BARGRAPH_FULL	(1 << 3)
#define BARGRAPH_16dB	(1 << 4)
#define MIXED			(1 << 5)
#define	CONFIG			(1 << 6)
//
#define	DEFAULT_MODE 	(1 << 0)			// Default Menu Mode

// USB Output Flags, used with [var_t].USB_Flags
#define USBPPOLL		(1 << 0)			// $pinst command last selected
#define USBPINST		(1 << 1)			// $pinst command last selected
#define	USBPPEP			(1 << 2)			// $ppep command last selected
#define USBPAVG			(1 << 3)			// $pavg command last selected
//						(1 << 4)			// not used
#define USBP_DB			(1 << 5)			// if set, then inst/pep/avg will be in dB.
#define USBPLONG		(1 << 6)			// $plong (inst, pep, avg) selected
#define	USBPCONT		(1 << 7)			// $pcont, continuous transmission of last selected


//-----------------------------------------------------------------------------
// Structures and Unions

typedef struct {
	int16_t	db10m;							// Calibrate, value in dBm x 10
	int16_t	ad;								// corresponding A/D value
} cal_t;

typedef struct 
{
		uint8_t		EEPROM_init_check;		// If value mismatch,
											// then update EEPROM to factory settings
		int16_t		encoderRes;				// (initial) Encoder resolution
		uint8_t		which_gainset;			// which gain setting is selected?
		int16_t		gainset[4];				// first one always zero, other 3: -70 to +70dB
		cal_t		calibrate[2];			// 2 Calibration points
		uint8_t		USB_data;				// Bool indicating whether output is being
											// transmitted over serial/USB
		uint8_t		USB_Flags;				// Bool indicating whether continuous output over
											// serial/USB (selected/deselected w USB commands $pcont/$ppoll)
		uint16_t	PEP_period;				// PEP envelope sampling time in 5ms increments (200 = 1 second)
		int16_t		AM_threshold;			// Threshold in dBm to display Amplitude Modulation
} var_t;


//-----------------------------------------------------------------------------
// Global variables
extern	EEMEM 		var_t E;				// Default Variables in EEPROM
extern				var_t R;				// Runtime Variables in Ram

extern 	int16_t		ad8307_ad;				// Measured A/D value from the AD8307
extern 	double 		ad8307_real;			// Calculated power measured by the AD8307
extern	double		power_db;				// Calculated power in dBm
extern	double		power_db_pep;			// Calculated PEP power in dBm
extern	double		power_db_avg;			// Calculated AVG power in dBm

extern	double		power_mw;				// Calculated power in mW
extern	double		power_mw_pep;			// Calculated PEP power in mW
extern	double		power_mw_avg;			// Calculated AVG power in mW

extern	int8_t		modulation_index;		// AM modulation index in %
extern	int16_t		power_snapshot_db10;	// A pushbutton snapshot of measured power, used for bargraph_16db

extern	int16_t		encOutput;				// Output From Encoder

extern	char		lcd_buf[];				// Used to process data to be passed to LCD and USB Serial

//-----------------------------------------------------------------------------
// Prototypes for functions
// PM.c

// PM_Encoder.c
extern void 		encoder_Init(void);				// Initialize the Rotary Encoder
extern void			encoder_Scan(void);				// Scan the Rotary Encoder

// Push Button and Rotary Encoder Menu functions
extern void			PushButtonMenu(void);

// PM_Print_Format__Functions.c
extern void			print_dbm(int16_t);
extern void			print_p_mw(double);
extern void			print_v(double);

// PM_Display_Functions.c
extern void			lcd_display_power_db(void);
extern void			lcd_display_power_w(void);
extern void			lcd_display_voltage(void);
extern void			lcd_display_bargraph_full(void);
extern void			lcd_display_bargraph_16db(void);
extern void			lcd_display_mixed(void);

// PM_USB_Serial.c
#ifdef WITH_USB
extern void			usb_poll_data(void);	// Write data to USB virtual serial port
extern void			usb_poll_inst(void);	// Write data to USB virtual serial port
extern void			usb_poll_pep(void);		// Write data to USB virtual serial port
extern void			usb_poll_avg(void);		// Write data to USB virtual serial port
extern void			usb_poll_instdb(void);	// Write data to USB virtual serial port
extern void			usb_poll_pepdb(void);	// Write data to USB virtual serial port
extern void			usb_poll_avgdb(void);	// Write data to USB virtual serial port
extern void			usb_poll_long(void);	// Write data to USB virtual serial port
extern void			usb_read_serial(void);	// Read incoming messages from USB bus
#endif

// LCD Bargraph stuff
extern void 		lcdProgressBarPeak(uint16_t, uint16_t, uint16_t, uint8_t);
extern void			lcd_bargraph_init(void);

#endif
