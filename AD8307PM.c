//*********************************************************************************
//**
//** Project.........: AD8307 based RF Power Meter
//**
//** Copyright (C) 2013  Loftur E. Jonasson  (tf3lj [at] arrl [dot] net)
//**
//** This program is free software: you can redistribute it and/or modify
//** it under the terms of the GNU General Public License as published by
//** the Free Software Foundation, either version 3 of the License, or
//** (at your option) any later version.
//**
//** This program is distributed in the hope that it will be useful,
//** but WITHOUT ANY WARRANTY; without even the implied warranty of
//** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//** GNU General Public License for more details.
//**
//** You should have received a copy of the GNU General Public License
//** along with this program.  If not, see <http://www.gnu.org/licenses/>.
//**
//** Platform........: AT90usb1286 @ 16MHz
//**
//** Initial version.: 2012-04-01, Loftur Jonasson, TF3LJ / VE2LJX
//**
//** Current version.: See PM.h
//**
//** History.........: ...
//**
//*********************************************************************************

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// Microcontroller Pin assignment (defined in the AD8307PM.h file):
//
// ------LCD Display------
// PC0 = LCD control RS
// PC1 = LCD control RW
// PC2 = LCD control E
// PC3
// PC4 = LCD D4
// PC5 = LCD D5
// PC6 = LCD D6
// PC7 = LCD D7
//
// ------Analog Input for Meter------
// PF0 = AD MUX0, input from AD8307
//
// ------Encoder and PushButton------
// PD4 = Push Button Selector input
// PD5 = Rotary Encoder A input
// PD7 = Rotary Encoder B input
//
// ------Debug LED------
// PD6 = LED output
//
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------


#include "AD8307PM.h"
#include "analog.h"
#include <math.h>


EEMEM		var_t	E;						// Variables in eeprom (user modifiable, stored)
			var_t	R						// Variables in ram/flash rom (default)
					=
					{
						COLDSTART_REF		// Update into eeprom if value mismatch
					, 	ENC_RES_DIVIDE		// Initial Encoder Resolution
					,	0					// Which GainPreset is selected (0,1,2,3) 0=0 or None
					, { 0 					// Gainset 0.  Always = 0
					, GAINSET1				// Three gain settings in dB x 10
					, GAINSET2              // Attenuators take a negative value
					, GAINSET3 }
					,{{ CAL1_NOR_VALUE
					   ,CAL1_RAW_DEFAULT}	// First Calibrate point, db*10 + AD value
					, { CAL2_NOR_VALUE
					   ,CAL2_RAW_DEFAULT}}	// Second Calibrate point, db*10 + AD value
					, USB_DATA				// Default USB Serial Data out setting
					, 0						// Bool USB_Flags, indicating which output command was requested last time
											// and whether Continuous Mode
					, PEP_PERIOD			// PEP envelope sampling time in 5ms increments (200 = 1 second)
					, AM_THRESHOLD			// Threshold in dBm to display Amplitude Modulation
					};

uint8_t		Status = 0;						// Contains various status flags
uint8_t		Menu_Mode;						// Menu Mode Flags

int16_t		ad8307_ad;						// Measured A/D value from the AD8307 as a 10 bit value
											// Resolution is Vref/1024, or 4.0mV, when 
											// a reference of 4.096V is used.

double		power_db;						// Calculated power in dBm
double		power_db_pep;					// Calculated PEP power in dBm
double		power_db_avg;					// Calculated AVG power in dBm

double		power_mw;						// Calculated power in mW
double		power_mw_pep;					// Calculated PEP power in mW
double		power_mw_avg;					// Calculated AVG power in mW

int8_t		modulation_index;				// AM modulation index in %
double		ad8307_real;					// Measured AD8307 power in dBm
int16_t		power_snapshot_db10;			// A pushbutton snapshot of measured power, used for bargraph_16db

const		uint8_t aref = ADC_METER_REF;	// Reference is defined in AD8307PM.h
											// (Normally an External 4.096V reference)

char lcd_buf[80];							// LCD print buffer


//
//-----------------------------------------------------------------------------------------
// 			Read ADC Mux
//-----------------------------------------------------------------------------------------
//
int16_t adc_Read(uint8_t mux)
{
    uint8_t low;

    ADCSRA = (1<<ADEN) | ADC_PRESCALER;             // enable ADC
    ADCSRB = (1<<ADHSM) | (mux & 0x20);             // high speed mode
    ADMUX = aref | (mux & 0x1F);                    // configure mux input
    ADCSRA = (1<<ADEN) | ADC_PRESCALER | (1<<ADSC); // start the conversion
    while (ADCSRA & (1<<ADSC)) ;                    // wait for result
    low = ADCL;                                     // must read LSB first
    return (ADCH << 8) | low;                       // must read MSB only once!
}


//
//-----------------------------------------------------------------------------------------
// 			Convert Voltage Reading into Power
//-----------------------------------------------------------------------------------------
//
void measure_Power(void)
{
	double 	delta_db;
	int16_t	delta_ad;
	double	delta_ad1db;

	// Calculate the slope gradient between the two calibration points:
	//
	// (dB_Cal1 - dB_Cal2)/(V_Cal1 - V_Cal2) = slope_gradient
	//
	delta_db = (double)((R.calibrate[1].db10m - R.calibrate[0].db10m)/10.0);
	delta_ad = R.calibrate[1].ad - R.calibrate[0].ad;
	delta_ad1db = delta_db/delta_ad;

	//
	// measured current dB value is then: (V - V_Cal1) * slope_gradient + dB_Cal1
	//
	ad8307_real = (ad8307_ad - R.calibrate[0].ad) * delta_ad1db + R.calibrate[0].db10m/10.0;
}


//
//-----------------------------------------------------------------------------------------
// 			Calculate all kinds of power
//-----------------------------------------------------------------------------------------
//
void calc_Power(void)
{
	#define	PEP_BUFFER	1000			// PEP Buffer size
	#define	AVG_BUF		200				// Buffer size for 1s Average measurement

	// For measurement of peak and average power
	static int16_t db_buff[PEP_BUFFER];	// dB voltage information in a one second window
	static uint16_t a=0;				// PEP ringbuffer counter
	static uint16_t b=0;				// 1s average ringbuffer counter
	int16_t max=-32767, min=32767;		// Keep track of Max and Min value within the PEP period
		
	// Instantaneous power, milliwatts and dBm
	power_mw = pow(10,ad8307_real/10.0);
	power_db = 10 * log10(power_mw);

	// Find peaks and averages
	// Multiply by 100 to make suitable for integer value
	// Store dB value in a ring buffer
	db_buff[a] =  100 * power_db;

	// Retrieve Max and Min Value within the PEP window
	for (uint16_t x = 0; x < R.PEP_period; x++)
	{
		if (min > db_buff[x]) min = db_buff[x];
		if (max < db_buff[x]) max = db_buff[x];
	}

	// PEP
	power_db_pep = max / 100.0;
	power_mw_pep = pow(10,power_db_pep/10.0);
		
	// The below sliding window routine to gather average power does not run fast enough 
	//(200x per second) to give stable and consistent readings.
	// It probably needs to run at a minimum of 5kHz to get reasonably good averaging.
	// Therefore this has been replaced with a max/min approximation routine.
	// Average power (1 second), milliwatts and dBm
	static double	p_buff[AVG_BUF];		// all instantaneous power measurements in 1s
	static double	p_plus;					// all power measurements within a 1s window added together
	p_buff[b] = power_mw;					// Add onto ring buffer
	p_plus = p_plus + power_mw;				// Add latest value to the total sum of all measurements in 1s
	if (b == AVG_BUF-1)						// Subtract oldest value in the 1s window
		p_plus = p_plus - p_buff[0];
	else
		p_plus = p_plus - p_buff[b+1];
	power_mw_avg = p_plus / (AVG_BUF-1);	// And finally, find the average
	power_db_avg = 10 * log10(power_mw_avg);
/*	
	// The above sliding window routine to gather average power does not run fast enough
	//(200x per second) to give stable and consistent readings, useful for Amplitude Modulation
	// measurements. It probably needs to run at a minimum of 5kHz to get reasonably good averaging.
	// Therefore this has been replaced with a max/min approximation routine.
	// Average power (1 second), milliwatts and dBm
	double v1, v2;
	v1 = sqrt(power_mw_pep);
	v2 = sqrt(power_mw_avg);
	modulation_index = 100 * (v1-v2) / v2;
*/
///*
	// Max/min approximation for average voltage and power
	//
	// Calculate max/min/average voltage and power
	double	v_min, v_max, v_avg;
	//double	p_min, p_max;
	v_min = pow(10,min/100.0/20.0);	// Normalize dB*100 and convert to Voltage
	v_max = pow(10,max/100.0/20.0);
	v_avg = (v_min + v_max) / 2.0;	// Average voltage in the presence of modulation
	// Average power (1s) mw and dBm
	//p_min = pow(10,min/100.0/10.0);	// Normalize dB*100 and convert to Power
	//p_max = pow(10,max/100.0/10.0);
	//power_mw_avg = (p_max+p_min) / 2.0;
	//power_db_avg = 10 * log10(power_mw_avg);
	// Amplitude Modulation index
	modulation_index = (int8_t) (100.0 * (v_max-v_avg)/v_avg);
//*/	
	// Advance PEP (1, 2.5 or 5s) and average (1s) ringbuffer counters
	a++, b++;
	if (a >= R.PEP_period) a = 0;
	if (b == AVG_BUF) b = 0;
}


//
//-----------------------------------------------------------------------------------------
// Top level task
// runs in an endless loop
//-----------------------------------------------------------------------------------------
//
void maintask(void)
{
	// Now we can do all kinds of business, such as measuring the AD8307 voltage output, 
	// scanning Rotary Encoder, updating LCD etc...
	static uint16_t lastIteration, lastIteration1, lastIteration2;	// Counters to keep track of time
	uint16_t Timer1val, Timer1val1, Timer1val2;		// Timers used for 100ms and 10ms polls
	static uint8_t pushcount=0;						// Measure push button time (max 2.5s)
	
	//-------------------------------------------------------------------------------
	// Here we do routines which are to be run through as often as possible
	// currently measured to be approximately once every 25 - 50 us
	//-------------------------------------------------------------------------------
	encoder_Scan();									// Scan the Rotary Encoder

	#if FAST_LOOP_THRU_LED							// Blink a LED every time when going through the main loop 
	LED_PORT = LED_PORT ^ LED;						// Blink a led
	#endif
	
	//-------------------------------------------------------------------------------
	// Here we do routines which are to be accessed once every approx 5ms
	// We have a free running timer which matures once every ~1.05 seconds
	//-------------------------------------------------------------------------------
	//Timer1val1 = TCNT1/328; // get current Timer1 value, changeable every ~5ms
	Timer1val1 = TCNT1/313;   // get current Timer1 value, changeable every ~5ms
	
	if (Timer1val1 != lastIteration1)				// Once every 5ms, do stuff
	{
		lastIteration1 = Timer1val1;				// Make ready for next iteration
		#if MS_LOOP_THRU_LED						// Blink LED every 5*2 ms, when going through the main loop 
		LED_PORT = LED_PORT ^ LED;  				// Blink a led
		#endif

		ad8307_ad = adc_Read(0);					// Measure voltage from AD8307
		measure_Power();							// Convert to Power in dBm
		calc_Power();								// Calculate Power, includes a 1 second
													// sliding window of the last 200 samples
	}

	//-------------------------------------------------------------------------------
	// Here we do routines which are to be accessed once every 1/100th of a second (10ms)
	// We have a free running timer which matures once every ~1.05 seconds
	//-------------------------------------------------------------------------------
	//Timer1val2 = TCNT1/656; // get current Timer1 value, changeable every ~1/100th sec
	Timer1val2 = TCNT1/626;   // get current Timer1 value, changeable every ~1/100th sec
	if (Timer1val2 != lastIteration2)				// Once every 1/100th of a second, do stuff
	{
		lastIteration2 = Timer1val2;				// Make ready for next iteration

		#if MED_LOOP_THRU_LED						// Blink LED every 10ms, when going through the main loop 
		LED_PORT = LED_PORT ^ LED;  				// Blink a led
		#endif
		
		// Nothing here
	}

	//-------------------------------------------------------------------------------
	// Here we do routines which are to be accessed once every 1/10th of a second
	// We have a free running timer which matures once every ~1.05 seconds
	//-------------------------------------------------------------------------------
	//Timer1val = TCNT1/6554; // get current Timer1 value, changeable every ~1/10th sec
	Timer1val = TCNT1/6253;   // get current Timer1 value, changeable every ~1/10th sec
	if (Timer1val != lastIteration)	// Once every 1/10th of a second, do stuff
	{
		lastIteration = Timer1val;					// Make ready for next iteration

		#if SLOW_LOOP_THRU_LED						// Blink LED every 100ms, when going through the main loop 
		LED_PORT = LED_PORT ^ LED;					// Blink a led
		#endif

		//-------------------------------------------------------------------
		// Read Encoder to cycle back and forth through modes
		//
		static int8_t current_mode = 0;			// Which display mode is active?
		#define MAX_MODES 4						// Number of available modes minus one

		// If the encoder was used while not in config mode:
		if ((!(Menu_Mode & CONFIG)) && (Status & ENC_CHANGE))
		{
			Status |= MODE_CHANGE + MODE_DISPLAY;// Used with LCD Display functions	
			
			// Mode switching travels only one click at a time, ignoring extra clicks
			if (encOutput > 0)
			{
				current_mode++;
				if (current_mode > MAX_MODES) current_mode = 0;
	    		// Reset data from Encoder
				Status &=  ~ENC_CHANGE;
				encOutput = 0;
			}
			else if (encOutput < 0)
			{
				current_mode--;
				if (current_mode < 0) current_mode = MAX_MODES;	
	  		  	// Reset data from Encoder
				Status &=  ~ENC_CHANGE;
				encOutput = 0;
			}
			switch (current_mode)
			{
					case 0:
						Menu_Mode = POWER_DB;
						break;
					case 1:
						Menu_Mode = POWER_W;
						break;
					case 2:
						Menu_Mode = VOLTAGE;
						break;
					case 3:
						Menu_Mode = BARGRAPH_FULL;
						break;
					case 4:
						Menu_Mode = BARGRAPH_16dB;
						break;
			}
		}
		
		//-------------------------------------------------------------------
		// Read Pushbutton state
		//
		// Enact Long Push (pushbutton has been held down for a certain length of time):
		if (pushcount >= ENC_PUSHB_MAX)				// "Long Push", goto Configuration Mode
		{
			Menu_Mode |= CONFIG;					// Switch into Configuration Menu, while
													// retaining memory of runtime function

			Status |= LONG_PUSH;					// Used with Configuration Menu functions	
			pushcount = 0;							// Initialize push counter for next time
		}
		// Enact Short Push (react on release if only short push)
		else if (ENC_PUSHB_INPORT & ENC_PUSHB_PIN) 	// Pin high = just released, or not pushed
		{
			// Do nothing if this is a release after Long Push
			if (Status & LONG_PUSH)					// Is this a release following a long push?
			{
					Status &= ~LONG_PUSH;			// Clear pushbutton status
			}
			// Do stuff on command
			else if (pushcount >= ENC_PUSHB_MIN)	// Check if this is more than a short spike
			{	
				if (Menu_Mode & CONFIG)
					Status |= SHORT_PUSH;			// Used with Configuration Menu functions	

				else
				{
					//
					// Various things to be done if short push... depending on which mode is active
					//
					power_snapshot_db10 = (int16_t) (power_db*10.0); // Used to center bargraph_16db
				} 
			}
			pushcount = 0;							// Initialize push counter for next time
		}
		else if (!(Status & LONG_PUSH))				// Button Pushed, count up the push timer
		{											// (unless this is tail end of a long push,
			pushcount++;							//  then do nothing)
		}

		//-------------------------------------------------------------------
		// Various Menu (rotary encoder) selectable display/function modes
		//
		if (Menu_Mode & CONFIG)					// Pushbutton Configuration Menu
		{
			PushButtonMenu();
		}	
		else if (Menu_Mode == POWER_DB)			// Power Meter in dBm
		{
			lcd_display_power_db();
		}
		else if (Menu_Mode == POWER_W)			// Power Meter in Watts
		{
			lcd_display_power_w();
		}
		else if (Menu_Mode == VOLTAGE)			// Power meter with Volt over 50 ohm
		{
			lcd_display_voltage();
		}
		else if (Menu_Mode == BARGRAPH_FULL)	// Bargraph meter
		{
			lcd_display_bargraph_full();
		}
		else if (Menu_Mode == BARGRAPH_16dB)	// Bargraph meter, 16dB full scale
		{
			lcd_display_bargraph_16db();
		}

#ifdef WITH_USB
		if (R.USB_data && (Status & USB_AVAILABLE))	// Handle USB serial port, if enabled and available
		{
			// If Continuous USB Send mode is selected, then send data every 100ms to computer
			// Only one of these is selected at any time
			if (R.USB_Flags & USBPCONT)
			{
				if (R.USB_Flags & USBPPOLL) usb_poll_data();	// Machine readable data
				else if (R.USB_Flags & USBP_DB)					// We want decibels
				{
					if (R.USB_Flags & USBPINST) usb_poll_instdb();		// Inst power, dB
					else if (R.USB_Flags & USBPPEP) usb_poll_pepdb();	// PEP, dB
					else if (R.USB_Flags & USBPAVG) usb_poll_avgdb();	// avg, dB
				}
				else if (R.USB_Flags & USBPINST) usb_poll_inst();		// Inst power
				else if (R.USB_Flags & USBPPEP ) usb_poll_pep();		// PEP
				else if (R.USB_Flags & USBPAVG ) usb_poll_avg();		// avg
				
				else if (R.USB_Flags & USBPLONG) usb_poll_long();		// Verbose message
			}
		}		
#endif
	}
	wdt_reset();								// Whoops... must remember to reset that running watchdog
}


//
//-----------------------------------------------------------------------------------------
// 			Setup Ports, timers, start the works and never return, unless reset
//								by the watchdog timer
//						then - do everything, all over again
//-----------------------------------------------------------------------------------------
//
int main(void)
{
//	DDRB |= (1<<PB2);
//	PORTB |= (1<<PB2);
	DDRB |= (1<<PB5); PORTB |= (1<<PB5);  for(;;);
	MCUSR &= ~(1 << WDRF);							// Disable watchdog if enabled by bootloader/fuses
	wdt_disable();

	clock_prescale_set(clock_div_1); 				// with 16MHz crystal this means CLK=16000000

	//------------------------------------------
	// 16-bit Timer1 Initialization
	TCCR1A = 0; //start the timer
	TCCR1B = (1 << CS12); // prescale Timer1 by CLK/256
	// 16000000 Hz / 256 = 62500 ticks per second
	// 16-bit = 2^16 = 65536 maximum ticks for Timer1
	// 65536 / 62500 = ~1.05 seconds
	// so Timer1 will overflow back to 0 about every 1 seconds
	// Timer1val = TCNT1; // get current Timer1 value

	//------------------------------------------
	// Init and set output for LED
	LED_DDR = LED;
	LED_PORT = 0;

	//------------------------------------------
	// Init Pushbutton input
	ENC_PUSHB_DDR = ENC_PUSHB_DDR & ~ENC_PUSHB_PIN;	// Set pin for input
	ENC_PUSHB_PORT= ENC_PUSHB_PORT | ENC_PUSHB_PIN;	// Set pull up

	//------------------------------------------
	// Set run time parameters to Factory default under certain conditions
	//
	// Enforce "Factory default settings" when firmware is run for the very first time after
	// a fresh firmware installation with a new "serial number" in the COLDSTART_REF #define
	// This may be necessary if there is garbage in the EEPROM, preventing startup
	// To activate, roll "COLDSTART_REF" Serial Number in the PM.h file
	if (eeprom_read_byte(&E.EEPROM_init_check) != R.EEPROM_init_check)
	{
		eeprom_write_block(&R, &E, sizeof(E));		// Initialize eeprom to "factory defaults".
	}
	else
	{
		eeprom_read_block(&R, &E, sizeof(E));		// Load the persistent data from eeprom
	}

   	lcdInit();										// Init the LCD

	// Initialize the LCD bargraph, load the bargraph custom characters
	lcd_bargraph_init();

	//------------------------------------------
	// LCD Print Version information (5 seconds)
	lcdClear();
	lcdGotoXY(0,0);
	lcdPrintData("AD8307 based",12);
	_delay_ms(500);
	lcdGotoXY(2,1);
	lcdPrintData("RF Power Meter",14);
	_delay_ms(1000);

	lcdGotoXY(0,0);
	lcdPrintData("Intelligent...",14);
	_delay_ms(500);
	lcdGotoXY(2,1);
	lcdPrintData("...Power Meter",14);
	_delay_ms(2000);

	lcdClear();
	lcdGotoXY(0,0);
	lcdPrintData("TF3LJ / VE2LJX",14);
	lcdGotoXY(11,1);
	sprintf(lcd_buf,"V%s", VERSION);
	lcdPrintData(lcd_buf, strlen(lcd_buf));

	_delay_ms(2000);

#ifdef WITH_USB
	if (R.USB_data)									// Enumerate USB serial port, if USB Serial Data enabled
	{
		usb_init();									// Initialize USB communications
		Status&=~USB_AVAILABLE;						// Disable USB communications until checked if actually available
	}
#endif
	
	wdt_enable(WDTO_1S);							// Start the Watchdog Timer, 1 sec

	encoder_Init();									// Init Rotary encoder

	Menu_Mode = DEFAULT_MODE;						// Power Meter Mode is normal default

	// Start the works, we're in business
	while (1)
	{
		maintask();									// Do useful stuff

#ifdef WITH_USB
		if (R.USB_data)								// Do the below if USB Port has been enabled
		{
			// If USB port is available and not busy, then use it - otherwise mark it as blocked.
			if (usb_configured() && (usb_serial_get_control() & USB_SERIAL_DTR))
			{
				Status |= USB_AVAILABLE;			// Enable USB communications
				//EXTLED_PORT |= EXT_G_LED;			// Turn Green LED On
				usb_read_serial();
			}
			else
			{
				Status&=~USB_AVAILABLE;				// Clear USB Available Flag to disable USB communications
				//EXTLED_PORT &= ~EXT_G_LED;		// Turn Green LED off, if previously on
			}
		}
#endif
	}
}
