CFILES=AD8307PM.c AD8307PM_Display_Functions.c AD8307PM_LCD_bargraph_lowlevel.c AD8307PM_PushButtonMenu.c AD8307PM_Encoder.c AD8307PM_Print_Format_Functions.c 
LIBCFILES=AVRLIB/lcd.c 
OFILES=AD8307PM_Display_Functions.o AD8307PM_Encoder.o AD8307PM_LCD_bargraph_lowlevel.o AD8307PM.o AD8307PM_Print_Format_Functions.o AD8307PM_PushButtonMenu.o AD8307PM_USBSerial.o
CFLAGS=-Os  -pedantic -std=gnu99
ELF=AD8307PM.elf
HEX=AD8307PM.hex

MCU=atmega328p
F_CPU=F_CPU=16000000UL

AVRDUDE_PROGRAMMERID=stk200  /* Programmer ids are listed in /etc/avrdude.conf */
AVRDUDE_PORT=/dev/ttyUSB4

ad8307pm: AD8307PM.c
	avr-gcc -g  -I AVRLIB/  -mmcu=$(MCU) -D$(F_CPU) $(CFLAGS) $(CFILES) $(LIBCFILES) -o $(ELF)
	avr-objcopy -j .text -j .data -O ihex $(ELF) $(HEX)

upload:
	avrdude -carduino  -p atmega328p -P /dev/ttyUSB1 -b57600 -U flash:w:AD8307PM.hex
