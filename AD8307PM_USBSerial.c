//*********************************************************************************
//**
//** Project.........: AD8307 based RF Power Meter
//**
//** Copyright (C) 2013  Loftur E. Jonasson  (tf3lj [at] arrl [dot] net)
//**
//** This program is free software: you can redistribute it and/or modify
//** it under the terms of the GNU General Public License as published by
//** the Free Software Foundation, either version 3 of the License, or
//** (at your option) any later version.
//**
//** This program is distributed in the hope that it will be useful,
//** but WITHOUT ANY WARRANTY; without even the implied warranty of
//** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//** GNU General Public License for more details.
//**
//** You should have received a copy of the GNU General Public License
//** along with this program.  If not, see <http://www.gnu.org/licenses/>.
//**
//** Platform........: AT90usb1286 @ 16MHz
//**
//** Initial version.: 2012-04-01, Loftur Jonasson, TF3LJ
//**
//**
//** History.........: Check the PM.c file
//**
//*********************************************************************************

#include 	<stdlib.h>
#include	"AD8307PM.h"

char incoming_command_string[50];			// Input from USB Serial


//
//-----------------------------------------------------------------------------------------
// 			Send AD8307 measurement data to the Computer
//-----------------------------------------------------------------------------------------
//
void usb_poll_data(void)
{
	// Note that no more than 256 chars can be handled during each pass
	// (or before each call to lufa_manage_serial()
	
	//------------------------------------------
	// Power indication, instantaneous power in raw format with 12 sub-decimals
	sprintf(lcd_buf,"%1.12f\r\n", power_mw/1000);
	usb_serial_write(lcd_buf,strlen(lcd_buf));
}
void usb_poll_inst(void)
{
	//------------------------------------------
	// Power indication, instantaneous power, formatted, pW-kW
	print_p_mw(power_mw);
	usb_serial_write(lcd_buf,strlen(lcd_buf));
	sprintf(lcd_buf, "\r\n");
	usb_serial_write(lcd_buf,strlen(lcd_buf));
}
void usb_poll_pep(void)
{
	//------------------------------------------
	// Power indication, PEP power, formatted, pW-kW
	print_p_mw(power_mw_pep);
	usb_serial_write(lcd_buf,strlen(lcd_buf));
	sprintf(lcd_buf, "\r\n");
	usb_serial_write(lcd_buf,strlen(lcd_buf));
}
void usb_poll_avg(void)
{
	//------------------------------------------
	// Power indication, 1s average power, formatted, pW-kW
	print_p_mw(power_mw_avg);
	usb_serial_write(lcd_buf,strlen(lcd_buf));
	sprintf(lcd_buf, "\r\n");
	usb_serial_write(lcd_buf,strlen(lcd_buf));
}

void usb_poll_instdb(void)
{
	//------------------------------------------
	// Power indication, instantaneous power, formatted, dB
	print_dbm((int16_t) (power_db*10.0));
	usb_serial_write(lcd_buf,strlen(lcd_buf));
	sprintf(lcd_buf, "\r\n");
	usb_serial_write(lcd_buf,strlen(lcd_buf));
}
void usb_poll_pepdb(void)
{
	//------------------------------------------
	// Power indication, PEP power, formatted, dB
	print_dbm((int16_t) (power_db_pep*10.0));
	usb_serial_write(lcd_buf,strlen(lcd_buf));
	sprintf(lcd_buf, "\r\n");
	usb_serial_write(lcd_buf,strlen(lcd_buf));
}
void usb_poll_avgdb(void)
{
	//------------------------------------------
	// Power indication, 1s average power, formatted, dB
	print_dbm((int16_t) (power_db_avg*10.0));
	usb_serial_write(lcd_buf,strlen(lcd_buf));
	sprintf(lcd_buf, "\r\n");
	usb_serial_write(lcd_buf,strlen(lcd_buf));
}

//
//-----------------------------------------------------------------------------------------
// 			Send AD8307 measurement data to the Computer
//
//			Long Human Readable Format

//-----------------------------------------------------------------------------------------
//
void usb_poll_long(void)
{
	// Note that no more than 256 chars can be handled during each pass
	// (or before each call to lufa_manage_serial()

	//------------------------------------------
	// Power indication, inst, peak (100ms), pep (1s), average (1s)
	sprintf(lcd_buf, "Power (inst, pep, avg):\r\n");
	usb_serial_write(lcd_buf,strlen(lcd_buf));
	print_p_mw(power_mw);
	usb_serial_write(lcd_buf,strlen(lcd_buf));
	usb_serial_write(", ",2);
	print_p_mw(power_mw_pep);
	usb_serial_write(lcd_buf,strlen(lcd_buf));
	usb_serial_write(", ",2);
	print_p_mw(power_mw_avg);
	usb_serial_write(lcd_buf,strlen(lcd_buf));
	sprintf(lcd_buf, "\r\n");
	usb_serial_write(lcd_buf,strlen(lcd_buf));
}


//
//-----------------------------------------------------------------------------------------
// 			Parse and act upon an incoming USB command
//
//			Implemented commands are:
//
//			$ppoll				Poll for one single USB serial report, inst power (unformatted)
//			$pinst				Poll for one single USB serial report, inst power (human readable)
//			$ppep				Poll for one single USB serial report, pep power (human readable)
//			$pavg				Poll for one single USB serial report, avg power (human readable)
//			$pinstdb			Poll for one single USB serial report, inst power in dB (human readable)
//			$ppepdb				Poll for one single USB serial report, pep power in dB (human readable)
//			$pavgdb				Poll for one single USB serial report, avg power in dB (human readable)
//			$plong				Poll for one single USB serial report, inst, pep, avg (long form)
//
//			$pcont				USB serial reporting in a continuous mode, 10 times per second
//
//								$ppoll, $pinst, $ppep, $pavg$ or $plong entered after $pcont 
//								will switch back to single shot mode
//
//			$calset cal1 AD1 cal2 AD2		Write new calibration values to the meter
//			$calget							Retrieve calibration values
//												where:
//											cal1 and cal2 are calibration setpoints 1 and 2 in 10x dBm
//												and
//											AD1 and AD2 are the corresponding AD values.
//
//			$encset x			x = Rotary Encoder Resolution, integer number, 1 to 8
//			$encget				Return current value
//
//			$amset x			x = minimum power to display Amplitude Modulation, -80 to 20 dBm
//								    (20 dBm effectively turns the function off)
//			$amget				Return current value
//
//			$version			Report version and date of firmware
//
//-----------------------------------------------------------------------------------------
//
void usb_parse_incoming(void)
{
	char *pEnd;
	int16_t inp_val;
		
	if (!strcmp("ppoll",incoming_command_string))			// Poll, if Continuous mode, then switch into Polled Mode
	{
		// Disable continuous USB report mode ($pcont) if previously set
		// and Write report mode to EEPROM, if changed
		if (!(R.USB_Flags & USBPPOLL)||(R.USB_Flags & USBPCONT))
		{
			R.USB_Flags = USBPPOLL;
			eeprom_write_block(&R.USB_Flags, &E.USB_Flags, sizeof(R.USB_Flags));		
		}
		usb_poll_data();									// Send data over USB
	}

	else if (!strcmp("pinst",incoming_command_string))	// Poll for one single Human Readable report
	{
		// Disable continuous USB report mode ($pcont) if previously set
		// and Write report mode to EEPROM, if changed
		if (!(R.USB_Flags & USBPINST)||(R.USB_Flags & USBPCONT))
		{
			R.USB_Flags = USBPINST;
			eeprom_write_block(&R.USB_Flags, &E.USB_Flags, sizeof(R.USB_Flags));
		}
		usb_poll_inst();
	}
	else if (!strcmp("ppep",incoming_command_string))	// Poll for one single Human Readable report
	{
		// Disable continuous USB report mode ($pcont) if previously set
		// and Write report mode to EEPROM, if changed
		if (!(R.USB_Flags & USBPPEP)||(R.USB_Flags & USBPCONT))
		{
			R.USB_Flags = USBPPEP;
			eeprom_write_block(&R.USB_Flags, &E.USB_Flags, sizeof(R.USB_Flags));
		}
		usb_poll_pep();
	}
	else if (!strcmp("pavg",incoming_command_string))	// Poll for one single Human Readable report
	{
		// Disable continuous USB report mode ($pcont) if previously set
		// and Write report mode to EEPROM, if changed
		if (!(R.USB_Flags & USBPAVG)||(R.USB_Flags & USBPCONT))
		{
			R.USB_Flags = USBPAVG;
			eeprom_write_block(&R.USB_Flags, &E.USB_Flags, sizeof(R.USB_Flags));
		}
		usb_poll_avg();
	}

	else if (!strcmp("pinstdb",incoming_command_string))	// Poll for one single Human Readable report
	{
		// Disable continuous USB report mode ($pcont) if previously set
		// and Write report mode to EEPROM, if changed
		if (((R.USB_Flags&(USBPINST|USBP_DB))  !=(USBPINST|USBP_DB)) || (R.USB_Flags & USBPCONT))
		{
			R.USB_Flags = USBPINST|USBP_DB;
			eeprom_write_block(&R.USB_Flags, &E.USB_Flags, sizeof(R.USB_Flags));
		}
		usb_poll_instdb();
	}
	else if (!strcmp("ppepdb",incoming_command_string))	// Poll for one single Human Readable report
	{
		// Disable continuous USB report mode ($pcont) if previously set
		// and Write report mode to EEPROM, if changed
		if (((R.USB_Flags&(USBPPEP|USBP_DB))!=(USBPPEP|USBP_DB)) || (R.USB_Flags & USBPCONT))
		{
			R.USB_Flags = USBPPEP|USBP_DB;
			eeprom_write_block(&R.USB_Flags, &E.USB_Flags, sizeof(R.USB_Flags));
		}
		usb_poll_pepdb();
	}
	else if (!strcmp("pavgdb",incoming_command_string))	// Poll for one single Human Readable report
	{
		// Disable continuous USB report mode ($pcont) if previously set
		// and Write report mode to EEPROM, if changed
		if (((R.USB_Flags&(USBPAVG|USBP_DB))!=(USBPAVG|USBP_DB)) || (R.USB_Flags & USBPCONT))
		{
			R.USB_Flags = USBPAVG|USBP_DB;
			eeprom_write_block(&R.USB_Flags, &E.USB_Flags, sizeof(R.USB_Flags));
		}
		usb_poll_avgdb();
	}

	else if (!strcmp("plong",incoming_command_string))	// Poll for one single Human Readable report
	{
		// Disable continuous USB report mode ($pcont) if previously set
		// and Write report mode to EEPROM, if changed
		if (!(R.USB_Flags & USBPLONG)||(R.USB_Flags & USBPCONT))
		{
			R.USB_Flags = USBPLONG;
			eeprom_write_block(&R.USB_Flags, &E.USB_Flags, sizeof(R.USB_Flags));
		}
		usb_poll_long();
	}
	else if (!strcmp("pcont",incoming_command_string))		// Switch into Continuous Mode
	{
		// Enable continuous USB report mode ($pcont) and write to EEPROM, if previously disabled
		if ((R.USB_Flags & USBPCONT) == 0)
		{
			R.USB_Flags |= USBPCONT;
			eeprom_write_block(&R.USB_Flags, &E.USB_Flags, sizeof(R.USB_Flags));
		}
	}
	else if (!strcmp("calget",incoming_command_string))	// Retrieve calibration values
	{
		sprintf(lcd_buf,"Cal: %4d,%4d,%4d,%4d",
			R.calibrate[0].db10m,R.calibrate[0].ad,R.calibrate[1].db10m,R.calibrate[1].ad);
		usb_serial_write(lcd_buf,strlen(lcd_buf));
	}
	else if (!strncmp("calset",incoming_command_string,6))	// Write new calibration values
	{
		R.calibrate[0].db10m = strtol(incoming_command_string+6,&pEnd,10);
		R.calibrate[0].ad = strtol(pEnd,&pEnd,10);
		R.calibrate[1].db10m = strtol(pEnd,&pEnd,10);
		R.calibrate[1].ad = strtol(pEnd,&pEnd,10);

		eeprom_write_block(&R.calibrate[0], &E.calibrate[0], sizeof (R.calibrate[0]));
		eeprom_write_block(&R.calibrate[1], &E.calibrate[1], sizeof (R.calibrate[1]));
	}
	else if (!strcmp("version",incoming_command_string))		// Poll for one single Human Readable report
	{
		sprintf(lcd_buf,"TF3LJ/VE2LJX AD8307 & AT90USB1286 based Power Meter\r\n");
		usb_serial_write(lcd_buf,strlen(lcd_buf));
		sprintf(lcd_buf,"Version "VERSION" "DATE"\r\n");
		usb_serial_write(lcd_buf,strlen(lcd_buf));
	}

	//	$pamset x		x = minimum power to display Amplitude Modulation, -80 to 20 dBm 
	//  (20 dBm effectively turns the function off)
	//	$pamget		Return current value
	else if (!strncmp("amset",incoming_command_string,5))
	{
		// Write value if valid
		inp_val = strtol(incoming_command_string+6,&pEnd,10);
		if ((inp_val>-80) && (inp_val<=20))
		{
			R.AM_threshold = inp_val;
			eeprom_write_block(&R.AM_threshold, &E.AM_threshold, sizeof (R.AM_threshold));
		}
	}
	else if (!strcmp("amget",incoming_command_string))
	{
		sprintf(lcd_buf,"Amplitude_Modulation_Threshold: %d\r\n",R.AM_threshold);
		usb_serial_write(lcd_buf,strlen(lcd_buf));
	}

	//	$pencset x		x = Rotary Encoder Resolution, integer number, 1 to 8
	//	$pencget		Return current value
	else if (!strncmp("encset",incoming_command_string,6))
	{
		// Write value if valid
		inp_val = strtol(incoming_command_string+7,&pEnd,10);
		if ((inp_val>0) && (inp_val<=8))
		{
			R.encoderRes = inp_val;
			eeprom_write_block(&R.encoderRes, &E.encoderRes, sizeof (R.encoderRes));
		}
	}
	else if (!strcmp("encget",incoming_command_string))
	{
		sprintf(lcd_buf,"Rotary_Encoder_Resolution: %u\r\n",R.encoderRes);
		usb_serial_write(lcd_buf,strlen(lcd_buf));
	}
}


//
//-----------------------------------------------------------------------------------------
// 			Monitor USB Serial port for an incoming USB command
//-----------------------------------------------------------------------------------------
//
void usb_read_serial(void)
{
	static uint8_t a;		// Indicates number of chars received in an incoming command
	static BOOL Incoming;
	uint8_t ReceivedChar;
	uint8_t waiting;		// Number of chars waiting in receive buffer
	
	//int16_t r;
	//uint8_t count=0;

	// Find out how many characters are waiting to be read.
	waiting = usb_serial_available();

	// Scan for a command attention symbol -> '$'
	if (waiting && (Incoming == FALSE))
	{
		ReceivedChar = usb_serial_getchar();
		// A valid incoming message starts with an "attention" symbol = '$'.
		// in other words, any random garbage received on USB port is ignored.
		if (ReceivedChar == '$')			// Start command symbol was received,
		{									// we can begin processing input command
			Incoming = TRUE;
			a = 0;
			waiting--;
		}
	}
	
	// Input command is on its way.  One or more characters are waiting to be read
	// and Incoming flag has been set. Read any available bytes from the USB OUT endpoint
	while (waiting && Incoming)
	{
		ReceivedChar = usb_serial_getchar();
		waiting--;

		if (a == sizeof(incoming_command_string)-1)	// Line is too long, discard input
		{
			Incoming = FALSE;
			a = 0;
		}
		// Check for End of line
		else if ((ReceivedChar=='\r') || (ReceivedChar=='\n'))
		{
			incoming_command_string[a] = 0;	// Terminate line
			usb_parse_incoming();			// Parse the command
			Incoming = FALSE;
			a = 0;
		}
		else								// Receive message, char by char
		{
			incoming_command_string[a] = ReceivedChar;
		}
		a++;								// String length count++
	}
}
