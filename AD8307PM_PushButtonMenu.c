//*********************************************************************************
//**
//** Project.........: AD8307 based RF Power Meter
//**
//** Copyright (C) 2013  Loftur E. Jonasson  (tf3lj [at] arrl [dot] net)
//**
//** This program is free software: you can redistribute it and/or modify
//** it under the terms of the GNU General Public License as published by
//** the Free Software Foundation, either version 3 of the License, or
//** (at your option) any later version.
//**
//** This program is distributed in the hope that it will be useful,
//** but WITHOUT ANY WARRANTY; without even the implied warranty of
//** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//** GNU General Public License for more details.
//**
//** You should have received a copy of the GNU General Public License
//** along with this program.  If not, see <http://www.gnu.org/licenses/>.
//**
//** Platform........: AT90usb1286 @ 16MHz
//**
//** Initial version.: 2012-04-01, Loftur Jonasson, TF3LJ
//**
//** History.........: ...
//**
//*********************************************************************************

#include "AD8307PM.h"
#include "lcd.h"
#include <stdio.h>


// First Level Menu Items
const uint8_t level0_menu_size = 8;
const char *level0_menu_items[] =
				{  "1-PEP period",
				   "2-Inp Gain Sel",
				   "3-Gain Adjust",
				   "4-Calibrate",
				   "5-Serial Data",
				   "6-Debug Disp",
				   "7-FactoryReset",
				 //"8-Adj Encoder Res",
				   "0-Exit" };

// Flag for PEP Sample Period select
#define PEP_MENU		1
// PEP Sample Period select menu Items
const uint8_t pep_menu_size = 3;
const char *pep_menu_items[] =
				{   "    1s",
					"  2.5s",
					"    5s"	};

// Flag for Reference 1 Menu Selection
//#define POW_MENU		2

// Flag for Set Gain values
#define GAIN_SEL_MENU		6
const uint8_t gain_sel_size = 6;
const char *gain_sel_items[] =
				{  "1-No Gain",
				   "2-GainPreset 1",
				   "3-GainPreset 2",
				   "4-GainPreset 3",
				   "9-Go Back",
				   "0-Exit"};

// Flag for Set Gain values
#define GAIN_MENU		7
const uint8_t gain_menu_size = 5;
const char *gain_menu_items[] =
				{  "1-GainPreset 1",
				   "2-GainPreset 2",
				   "3-GainPreset 3",
				   "9-Go Back",
				   "0-Exit"};

// Flags for Gain Submenu functions
#define GAIN_SET1_MENU	701
#define GAIN_SET2_MENU	702
#define GAIN_SET3_MENU	703

// Flag for Encoder Resolution Change
//#define ENCODER_MENU	8

// Flag for Calibrate menu
#define CAL_MENU		9
const uint8_t calibrate_menu_size = 5;
const char *calibrate_menu_items[] =
				{  "1-Single (dBm)",
				   "2-First  (dBm)",
				   "3-Second (dBm)",
				   "9-Go Back",
				   "0-Exit"};

// Flags for Calibrate Submenu functions
#define CAL_SET0_MENU	900	// single level calibration
#define CAL_SET1_MENU	901	// 1st level
#define CAL_SET2_MENU	902	// second level

// Flag for Serial Data Out
#define SERIAL_MENU		10
// Serial menu Items
const uint8_t serial_menu_size = 2;
const char *serial_menu_items[] =
				{   "1-Off",
					"2-On"	};

// Flag for Debug Screen
#define DEBUG_MENU		11

// Flag for Factory Reset
#define FACTORY_MENU	12
// Factory Reset menu Items
const uint8_t factory_menu_size = 3;
const char *factory_menu_items[] =
				{  "1-No - Go back",
				   "2-Yes - Reset",
				   "0-No - Exit" };


uint16_t		menu_level = 0;						// Keep track of which menu we are in
uint8_t			menu_data = 0;						// Pass data to lower menu

int8_t			gain_selection;						// keep track of which GainPreset is currently selected

char			lcd_buf[20];						// LCD print buffer

//----------------------------------------------------------------------
// Display a Menu of choices, one line at a time
//
// **menu refers to a pointer array containing the Menu to be printed
//
// menu_size indicates how many pointers (menu items) there are in the array
//
// current_choice indicates which item is currently up for selection if
// pushbutton is pushed
//
// row & col are the coordinates for the line to be printed
//
//----------------------------------------------------------------------
void lcd_scroll_Menu(char **menu, uint8_t menu_size,
		uint8_t current_choice, uint8_t row, uint8_t col)
{
	uint8_t a;


	// Clear LCD from begin_col to end of line.
	lcdGotoXY(col, row);
	for (a = col; a < 16; a++)
	lcdDataWrite(' ');

	// Using Menu list pointed to by **menu, preformat for print:
	// First line contains previous choice, secon line contains
	// current choice preceded with a '->', and third line contains
	// next choice
	lcdGotoXY(col, row);
	sprintf(lcd_buf,"->%s", *(menu + current_choice));
	lcdPrintData(lcd_buf, strlen(lcd_buf));
}


//----------------------------------------------------------------------
// Menu functions begin:
//----------------------------------------------------------------------


//--------------------------------------------------------------------
// Debug Screen, exit on push
//--------------------------------------------------------------------
void debug_menu(void)
{
	lcd_display_mixed();		// Display diverse debug stuff

	// Exit on Button Push
	if (Status & SHORT_PUSH)
	{
		Status &=  ~SHORT_PUSH;	// Clear pushbutton status

		lcdClear();
		lcdGotoXY(0,1);
		lcdPrintData("Nothing Changed",15);
		_delay_ms(200);
		Menu_Mode |= CONFIG;	// We're NOT done, just backing off
		menu_level = 0;			// We are done with this menu level
	}
}


//--------------------------------------------------------------------
// Peak Envelope Power (PEP) period selection Menu
//--------------------------------------------------------------------
void pep_menu(void)
{
	static int8_t	current_selection;
	static uint8_t	LCD_upd = FALSE;	// Keep track of LCD update requirements
	
	// Get Current value
	if (R.PEP_period == 500) current_selection = 1;			// 2.5 seconds
	else if (R.PEP_period == 1000) current_selection = 2;	// 5 seconds
	else current_selection = 0;								// Any other value, other than 1s, is invalid
	
	// Selection modified by encoder.  We remember last selection, even if exit and re-entry
	if (Status & ENC_CHANGE)
	{
		if (encOutput > 0)
		{
			current_selection++;
		}
		else if (encOutput < 0)
		{
			current_selection--;
		}
		// Reset data from Encoder
		Status &=  ~ENC_CHANGE;
		encOutput = 0;

		// Indicate that an LCD update is needed
		LCD_upd = FALSE;
	}

	// If LCD update is needed
	if (LCD_upd == FALSE)
	{
		LCD_upd = TRUE;					// We have serviced LCD

		// Keep Encoder Selection Within Bounds of the Menu Size
		uint8_t menu_size = pep_menu_size;
		while(current_selection >= menu_size)
		current_selection -= menu_size;
		while(current_selection < 0)
		current_selection += menu_size;

		if      (current_selection == 1) R.PEP_period = 500;
		else if (current_selection == 2) R.PEP_period = 1000;
		else R.PEP_period = 200;
		
		lcdClear();
		lcdGotoXY(0,0);
		lcdPrintData("PEP period:",11);
		lcdGotoXY(0,1);
		lcdPrintData("Select",6);

		// Print the Rotary Encoder scroll Menu
		lcd_scroll_Menu((char**)pep_menu_items, menu_size, current_selection, 1, 6);
	}

	// Enact selection
	if (Status & SHORT_PUSH)
	{
		lcdClear();
		lcdGotoXY(0,1);
		
		Status &=  ~SHORT_PUSH;			// Clear pushbutton status
		
		// Check if selected threshold is not same as previous
		if (eeprom_read_word(&E.PEP_period) != R.PEP_period)
		{
			eeprom_write_block(&R.PEP_period, &E.PEP_period, sizeof (R.PEP_period));
			lcdPrintData("Value Stored",12);
		}
		else lcdPrintData("Nothing Changed",15);

		_delay_ms(200);
		Menu_Mode &= ~CONFIG;			// We're done with Menu, EXIT
		menu_level = 0;					// We are done with this menu level
		LCD_upd = FALSE;				// Make ready for next time
	}
}


//--------------------------------------------------------------------
// Gain Preset Select Menu functions
//--------------------------------------------------------------------
void gain_select_menu(void)
{
	static uint8_t LCD_upd = FALSE;		// Keep track of LCD update requirements

	// Selection modified by encoder.  We remember last selection, even if exit and re-entry
	if (Status & ENC_CHANGE)
	{
		if (encOutput > 0)
		{
			gain_selection++;
	    	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		else if (encOutput < 0)
		{
			gain_selection--;
	  	  	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		// Indicate that an LCD update is needed
		LCD_upd = FALSE;
	}

	// If LCD update is needed
	if (LCD_upd == FALSE)
	{
		LCD_upd = TRUE;					// We have serviced LCD

		// Keep Encoder Selection Within Bounds of the Menu Size
		uint8_t menu_size = gain_sel_size;
		while(gain_selection >= menu_size)
			gain_selection -= menu_size;
		while(gain_selection < 0)
			gain_selection += menu_size;

		lcdClear();
		
		// Print the Rotary Encoder scroll Menu
		lcd_scroll_Menu((char**)gain_sel_items, menu_size, gain_selection,0, 0);

		// Indicate Currently selected Gain Preset
		lcdGotoXY(0,1);
		lcdPrintData("Select->   ",11);

		if (gain_selection < 4)
		{

			int16_t value=0;

			switch (gain_selection)
			{
				case 0:
					value = 0;	// No gain
					break;
				default:
					value = R.gainset[gain_selection];
					break;
			}

			// Format and print current value
			int16_t val_sub = value;
			int16_t val = val_sub / 10;
			val_sub = val_sub % 10;
			if (value < 0)
			{
				val*=-1;
				val_sub*=-1;
				sprintf(lcd_buf,"-%1u.%01u",val, val_sub);
				lcdPrintData(lcd_buf, strlen(lcd_buf));			}
			else
			{
				sprintf(lcd_buf,"%2u.%01u",val, val_sub);
				lcdPrintData(lcd_buf, strlen(lcd_buf));
			}
		}
		else
		{
			lcdPrintData(" --",3);
		}
	}

	// Enact selection by saving in EEPROM
	if (Status & SHORT_PUSH)
	{
		Status &=  ~SHORT_PUSH;			// Clear pushbutton status

		lcdClear();
		lcdGotoXY(1,1);				

		if (gain_selection < 4)			// Gain Preset was selected
		{
			R.which_gainset = gain_selection;
			eeprom_write_block(&R.which_gainset, &E.which_gainset, sizeof (R.which_gainset));
			lcdPrintData("Value Stored",12);
		}
		else lcdPrintData("Nothing Changed",15);

		_delay_ms(200);

		if (gain_selection == 5)		// Exit selected
		{
			Menu_Mode &= ~CONFIG;		// We're done, EXIT
		}
		else Menu_Mode |= CONFIG;		// We're NOT done, just backing off

		menu_level = 0;					// We are done with this menu level
		LCD_upd = FALSE;				// Make ready for next time
	}
}

//--------------------------------------------------------------------
// Gain Preset Submenu functions
//--------------------------------------------------------------------
void gain_menu_level2(void)
{
	static int16_t	current_selection;	// Keep track of current LCD menu selection
	static uint8_t LCD_upd = FALSE;		// Keep track of LCD update requirements

	// Get Current value
	if      (menu_level == GAIN_SET1_MENU) current_selection = R.gainset[1];
	else if (menu_level == GAIN_SET2_MENU) current_selection = R.gainset[2];
	else if (menu_level == GAIN_SET3_MENU) current_selection = R.gainset[3];


	// Selection modified by encoder.  We remember last selection, even if exit and re-entry
	if (Status & ENC_CHANGE)
	{
		if (encOutput > 0)
		{
			current_selection++;
	    	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		else if (encOutput < 0)
		{
			current_selection--;
	  	  	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		// Indicate that an LCD update is needed
		LCD_upd = FALSE;
	}

	// If LCD update is needed
	if (LCD_upd == FALSE)
	{
		LCD_upd = TRUE;					// We have serviced LCD

		// Keep Encoder Selection Within Bounds of the Menu Size
		int16_t max_value = 700;		// Highest Gain value in dB * 10
		int16_t min_value = -700;		// Lowest Gain value in dB * 10
		if(current_selection > max_value) current_selection = max_value;
		if(current_selection < min_value) current_selection = min_value;

		// Store Current value in running storage
		if      (menu_level == GAIN_SET1_MENU) R.gainset[1] = current_selection;
		else if (menu_level == GAIN_SET2_MENU) R.gainset[2] = current_selection;
		else if (menu_level == GAIN_SET3_MENU) R.gainset[3] = current_selection;

		lcdClear();
		lcdGotoXY(0,0);	

		if (menu_level == GAIN_SET1_MENU)
		{
			lcdPrintData("Gain Preset 1:",14);
		}
		else if (menu_level == GAIN_SET2_MENU)
		{
			lcdPrintData("Gain Preset 2:",14);
		}
		else if (menu_level == GAIN_SET3_MENU)
		{
			lcdPrintData("Gain Preset 3:",14);
		}
		lcdGotoXY(0,1);
		lcdPrintData("Adjust>  ",9);
		// Format and print current value
		int16_t val_sub = current_selection;
		int16_t val = val_sub / 10;
		val_sub = val_sub % 10;
		if (current_selection < 0)
		{
			val*=-1;
			val_sub*=-1;
			sprintf(lcd_buf,"-%1u.%01udB",val, val_sub);
		}
		else
		{
			sprintf(lcd_buf," %2u.%01udB",val, val_sub);
		}
		lcdPrintData(lcd_buf, strlen(lcd_buf));
	}

	// Enact selection by saving in EEPROM
	if (Status & SHORT_PUSH)
	{
		Status &=  ~SHORT_PUSH;			// Clear pushbutton status

		// Save modified value
		if (menu_level == GAIN_SET1_MENU)
		{
			eeprom_write_block(&R.gainset[1], &E.gainset[1], sizeof (R.gainset[1]));
		}
		else if (menu_level == GAIN_SET2_MENU)
		{
			eeprom_write_block(&R.gainset[2], &E.gainset[2], sizeof (R.gainset[2]));
		}
		else if (menu_level == GAIN_SET3_MENU)
		{
			eeprom_write_block(&R.gainset[3], &E.gainset[2], sizeof (R.gainset[3]));
		}

		Status &=  ~SHORT_PUSH;			// Clear pushbutton status
		lcdClear();
		lcdGotoXY(1,1);				
		lcdPrintData("Value Stored",12);
		_delay_ms(200);
		Menu_Mode |= CONFIG;			// We're NOT done, just backing off
		menu_level = GAIN_MENU;			// We are done with this menu level
		LCD_upd = FALSE;				// Make ready for next time
	}
}



//--------------------------------------------------------------------
// Gain Menu functions
//--------------------------------------------------------------------
void gain_set_menu(void)
{
	static int8_t	current_selection;	// Keep track of current LCD menu selection
	static uint8_t LCD_upd = FALSE;		// Keep track of LCD update requirements

	// Selection modified by encoder.  We remember last selection, even if exit and re-entry
	if (Status & ENC_CHANGE)
	{
		if (encOutput > 0)
		{
			current_selection++;
	    	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		else if (encOutput < 0)
		{
			current_selection--;
	  	  	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		// Indicate that an LCD update is needed
		LCD_upd = FALSE;
	}

	// If LCD update is needed
	if (LCD_upd == FALSE)
	{
		LCD_upd = TRUE;					// We have serviced LCD

		// Keep Encoder Selection Within Bounds of the Menu Size
		uint8_t menu_size = gain_menu_size;
		while(current_selection >= menu_size)
			current_selection -= menu_size;
		while(current_selection < 0)
			current_selection += menu_size;

		lcdClear();
		
		// Print the Rotary Encoder scroll Menu
		lcd_scroll_Menu((char**)gain_menu_items, menu_size, current_selection,1, 0);

		// Indicate Current value stored under the currently selected Gain Preset
		// The "stored" value indication changes according to which Preset is currently selected.
		lcdGotoXY(0,0);				
		lcdPrintData("AdjustGain:",11);
		if (current_selection < 3)
		{
			int16_t value;

			value = R.gainset[current_selection+1];

			int16_t val_sub = value;
			int16_t val = val_sub / 10;
			val_sub = val_sub % 10;

			// Print value of currently indicated gain Preset
			if (value < 0)
			{
				val*=-1;
				val_sub*=-1;
				sprintf(lcd_buf,"-%1u.%01u",val, val_sub);
			}
			else
			{
				sprintf(lcd_buf," %2u.%01u",val, val_sub);
			}
			lcdPrintData(lcd_buf, strlen(lcd_buf));
		}
		else
		{
			lcdPrintData(" --",3);
		}
	}

	// Enact selection
	if (Status & SHORT_PUSH)
	{
		Status &=  ~SHORT_PUSH;			// Clear pushbutton status

		switch (current_selection)
		{
			case 0:
				menu_level = GAIN_SET1_MENU;
				LCD_upd = FALSE;	// force LCD reprint
				break;
			case 1:
				menu_level = GAIN_SET2_MENU;
				LCD_upd = FALSE;	// force LCD reprint
				break;
			case 2:
				menu_level = GAIN_SET3_MENU;
				LCD_upd = FALSE;	// force LCD reprint
				break;
			case 3:
				lcdClear();
				lcdGotoXY(1,1);				
				lcdPrintData("Done w. Gain",12);
				_delay_ms(200);
				Menu_Mode |= CONFIG;	// We're NOT done, just backing off
				menu_level = 0;			// We are done with this menu level
				LCD_upd = FALSE;		// Make ready for next time
				break;
			default:
				lcdClear();
				lcdGotoXY(1,1);				
				lcdPrintData("Done w. Gain",12);
				_delay_ms(200);
				Menu_Mode &=  ~CONFIG;	// We're done
				menu_level = 0;			// We are done with this menu level
				LCD_upd = FALSE;		// Make ready for next time
				break;
		}
	}
}

/*
//--------------------------------------------------------------------
// Rotary Encoder Resolution
//--------------------------------------------------------------------
void encoder_menu(void)
{

	uint8_t	current_selection;			// Keep track of current Encoder Resolution

	static uint8_t LCD_upd = FALSE;		// Keep track of LCD update requirements

	// Get Current value
	current_selection = R.encoderRes;

	// Selection modified by encoder.  We remember last selection, even if exit and re-entry
	if (Status & ENC_CHANGE)
	{
		if (encOutput > 0)
		{
			current_selection=current_selection<<1;
	    	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		else if (encOutput < 0)
		{
			current_selection=current_selection>>1;
	  	  	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		// Indicate that an LCD update is needed
		LCD_upd = FALSE;
	}

	// If LCD update is needed
	if (LCD_upd == FALSE)
	{
		LCD_upd = TRUE;					// We have serviced LCD

		// Keep Encoder Selection Within Bounds of the Menu Size
		if(current_selection > 128) current_selection = 128;
		if(current_selection < 1) current_selection = 1;

		// Store Current value in running storage
		R.encoderRes = current_selection;

		lcd_clrscr();
		lcd_gotoxy(0,0);	
		lcd_puts_P("Encoder ResDivide:");

		lcd_gotoxy(0,1);
		lcd_puts_P("Rotate to Adjust");
		lcd_gotoxy(0,2);
		lcd_puts_P("Push to Save");
		// Format and print current value
		lcd_gotoxy(0,3);
		lcd_puts_P("->");

		int16_t val = current_selection;
		rprintf("%3u",val);
	}

	// Enact selection by saving in EEPROM
	if (Status & SHORT_PUSH)
	{
		Status &=  ~SHORT_PUSH;			// Clear pushbutton status

		// Save modified value
		eeprom_write_block(&R.encoderRes, &E.encoderRes, sizeof (R.encoderRes));

		Status &=  ~SHORT_PUSH;			// Clear pushbutton status
		lcd_clrscr();
		lcd_gotoxy(1,1);				
		lcd_puts_P("Value Stored");
		_delay_ms(200);
		Menu_Mode |= CONFIG;			// We're NOT done, just backing off
		menu_level = 0;					// We are done with this menu level
		LCD_upd = FALSE;				// Make ready for next time
	}
}
*/

//--------------------------------------------------------------------
// Calibrate Submenu functions
//--------------------------------------------------------------------
void calibrate_menu_level2(void)
{
	static int16_t	current_selection;	// Keep track of current LCD menu selection
	static uint8_t LCD_upd = FALSE;		// Keep track of LCD update requirements

	uint8_t cal_set;					// Determine whether CAL_SET0, CAL_SET1 or CAL_SET2

	if (menu_level == CAL_SET2_MENU) cal_set = 1;	// CAL_SET2_MENU
	else cal_set = 0;								// CAL_SET0_MENU or CAL_SET1_MENU

	// Get Current value
	current_selection = R.calibrate[cal_set].db10m;

	// Selection modified by encoder.  We remember last selection, even if exit and re-entry
	if (Status & ENC_CHANGE)
	{
		if (encOutput > 0)
		{
			current_selection++;
	    	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		else if (encOutput < 0)
		{
			current_selection--;
	  	  	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		// Indicate that an LCD update is needed
		LCD_upd = FALSE;
	}

	// If LCD update is needed
	if (LCD_upd == FALSE)
	{
		LCD_upd = TRUE;					// We have serviced LCD

		// Keep Encoder Selection Within Bounds of the Menu Size
		int16_t max_value = 100;		// Highest permissible Calibration value in dB * 10
		int16_t min_value = -500;		// Lowest permissible Calibration value in dB * 10
		if(current_selection > max_value) current_selection = max_value;
		if(current_selection < min_value) current_selection = min_value;

		// Store Current value in running storage
		R.calibrate[cal_set].db10m = current_selection;

		lcdClear();
		lcdGotoXY(0,0);	

		if (menu_level == CAL_SET0_MENU)
		{
			lcdPrintData("One CalSetPoint:",16);
		}
		if (menu_level == CAL_SET1_MENU)
		{
			lcdPrintData("1st CalSetPoint:",16);
		}
		else if (menu_level == CAL_SET2_MENU)
		{
			lcdPrintData("2nd CalSetPoint:",16);
		}

		lcdGotoXY(0,1);
		lcdPrintData("Adjust>",7);
		// Format and print current value
		int16_t val_sub = current_selection;
		int16_t val = val_sub / 10;
		val_sub = val_sub % 10;
		if (current_selection < 0)
		{
			val*=-1;
			val_sub*=-1;
			sprintf(lcd_buf," -%1u.%01udBm",val, val_sub);
		}
		else
		{
			sprintf(lcd_buf," %2u.%01udBm",val, val_sub);
		}
		lcdPrintData(lcd_buf, strlen(lcd_buf));
	}

	// Enact selection by saving in EEPROM
	if (Status & SHORT_PUSH)
	{
		Status &=  ~SHORT_PUSH;			// Clear pushbutton status

		// Save modified value
		if (menu_level == CAL_SET0_MENU)
		{
			R.calibrate[0].ad = ad8307_ad;
			eeprom_write_block(&R.calibrate[0], &E.calibrate[0], sizeof (R.calibrate[0]));
			// Set second calibration point at 30 dB less, assuming 25mV per dB
			R.calibrate[1].db10m = R.calibrate[0].db10m - 400;
			// Value to subtract is dependent on A/D Voltage reference and AD8307 slope
			R.calibrate[1].ad = ad8307_ad - 400.0 * REF_SLOPE;
			eeprom_write_block(&R.calibrate[1], &E.calibrate[1], sizeof (R.calibrate[1]));
		}
		else if (menu_level == CAL_SET1_MENU)
		{
			R.calibrate[0].ad = ad8307_ad;
			eeprom_write_block(&R.calibrate[0], &E.calibrate[0], sizeof (R.calibrate[0]));
		}
		else if (menu_level == CAL_SET2_MENU)
		{
			R.calibrate[1].ad = ad8307_ad;
			eeprom_write_block(&R.calibrate[1], &E.calibrate[1], sizeof (R.calibrate[1]));
		}


		Status &=  ~SHORT_PUSH;			// Clear pushbutton status
		lcdClear();
		lcdGotoXY(1,1);				
		lcdPrintData("Value Stored",12);
		_delay_ms(200);
		if (menu_level == CAL_SET0_MENU)
		{
			Menu_Mode &= ~CONFIG;		// We're done, Exit menu
			menu_level = 0;				// We are done, Exit Config
		}
		else
		{
			Menu_Mode |= CONFIG;		// We're NOT done, just backing off
			menu_level = CAL_MENU;		// We are done with this menu level
		}
		LCD_upd = FALSE;				// Make ready for next time
	}
}



//--------------------------------------------------------------------
// Calibrate Menu functions
//--------------------------------------------------------------------
void calibrate_menu(void)
{
	static int8_t	current_selection;	// Keep track of current LCD menu selection
	static uint8_t LCD_upd = FALSE;		// Keep track of LCD update requirements

	// Selection modified by encoder.  We remember last selection, even if exit and re-entry
	if (Status & ENC_CHANGE)
	{
		if (encOutput > 0)
		{
			current_selection++;
	    	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		else if (encOutput < 0)
		{
			current_selection--;
	  	  	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		// Indicate that an LCD update is needed
		LCD_upd = FALSE;
	}

	// If LCD update is needed
	if (LCD_upd == FALSE)
	{
		LCD_upd = TRUE;					// We have serviced LCD

		// Keep Encoder Selection Within Bounds of the Menu Size
		uint8_t menu_size = calibrate_menu_size;
		while(current_selection >= menu_size)
			current_selection -= menu_size;
		while(current_selection < 0)
			current_selection += menu_size;

		lcdClear();
		
		// Print the Rotary Encoder scroll Menu
		lcd_scroll_Menu((char**)calibrate_menu_items, menu_size, current_selection,1, 0);

		// Indicate Current value stored under the currently selected GainPreset
		// The "stored" value indication changes according to which GainPreset is currently selected.
		lcdGotoXY(0,0);				
		lcdPrintData("Calibrate",9);
	
		if (current_selection < 3)
		{
			int16_t value=0;

			switch (current_selection)
			{
				case 0:
				case 1:
					value = R.calibrate[0].db10m;
					break;
				case 2:
					value = R.calibrate[1].db10m;
					break;
			}
			int16_t val_sub = value;
			int16_t val = val_sub / 10;
			val_sub = val_sub % 10;

			// Print value of currently indicated reference
			lcdGotoXY(11,0);
			if (value < 0)
			{
				val*=-1;
				val_sub*=-1;
				sprintf(lcd_buf,"-%1u.%01u",val, val_sub);
			}
			else
			{
				sprintf(lcd_buf,"%2u.%01u",val, val_sub);
			}
			lcdPrintData(lcd_buf, strlen(lcd_buf));
		}
		else
		{
			lcdGotoXY(11,0);
			lcdPrintData(" --",3);
		}
	}

	// Enact selection
	if (Status & SHORT_PUSH)
	{
		Status &=  ~SHORT_PUSH;			// Clear pushbutton status

		switch (current_selection)
		{
			case 0:
				menu_level = CAL_SET0_MENU;
				LCD_upd = FALSE;	// force LCD reprint
				break;
			case 1:
				menu_level = CAL_SET1_MENU;
				LCD_upd = FALSE;	// force LCD reprint
				break;
			case 2:
				menu_level = CAL_SET2_MENU;
				LCD_upd = FALSE;	// force LCD reprint
				break;
			case 3:
				lcdClear();
				lcdGotoXY(1,1);				
				lcdPrintData("Done w. Cal",11);
				_delay_ms(200);
				Menu_Mode |= CONFIG;	// We're NOT done, just backing off
				menu_level = 0;			// We are done with this menu level
				LCD_upd = FALSE;		// Make ready for next time
				break;
			default:
				lcdClear();
				lcdGotoXY(1,1);				
				lcdPrintData("Done w. Cal",11);
				_delay_ms(200);
				Menu_Mode &=  ~CONFIG;	// We're done
				menu_level = 0;			// We are done with this menu level
				LCD_upd = FALSE;		// Make ready for next time
				break;
		}
	}
}


//--------------------------------------------------------------------
// USB Serial Data output ON/OFF
//--------------------------------------------------------------------
void serial_menu(void)
{
	static int8_t	current_selection;
	static uint8_t	LCD_upd = FALSE;		// Keep track of LCD update requirements

	current_selection = R.USB_data;
	
	// Selection modified by encoder.  We remember last selection, even if exit and re-entry
	if (Status & ENC_CHANGE)
	{
		if (encOutput > 0)
		{
			current_selection++;
		}
		else if (encOutput < 0)
		{
			current_selection--;
		}
		// Reset data from Encoder
		Status &=  ~ENC_CHANGE;
		encOutput = 0;

		// Indicate that an LCD update is needed
		LCD_upd = FALSE;
	}

	// If LCD update is needed
	if (LCD_upd == FALSE)
	{
		LCD_upd = TRUE;					// We have serviced LCD

		// Keep Encoder Selection Within Bounds of the Menu Size
		uint8_t menu_size = serial_menu_size;
		while(current_selection >= menu_size)
		current_selection -= menu_size;
		while(current_selection < 0)
		current_selection += menu_size;

		R.USB_data = current_selection;
		
		lcdClear();
		lcdGotoXY(0,0);
		lcdPrintData("USB Serial Data:",16);

		// Print the Rotary Encoder scroll Menu
		lcd_scroll_Menu((char**)serial_menu_items, menu_size, current_selection, 1, 0);
	}

	// Enact selection
	if (Status & SHORT_PUSH)
	{
		Status &=  ~SHORT_PUSH;			// Clear pushbutton status

		// Check if selected threshold is not same as previous
		if (eeprom_read_byte(&E.USB_data) != R.USB_data)
		{
			eeprom_write_block(&R.USB_data, &E.USB_data, sizeof (R.USB_data));
			lcdGotoXY(0,0);
			lcdPrintData("Value Stored",12);
			_delay_ms(200);
			lcdClear();
			lcdGotoXY(0,1);
			if (R.USB_data==0)
			lcdPrintData("Serial Data OFF",15);
			else
			lcdPrintData("Serial Data ON",14);
			_delay_ms(200);
			asm volatile("jmp 0x00000");	// Soft Reset
			//while (1);					// Bye bye, Death by Watchdog
			//								// BUG, Watchdog is unreliable
			//								// problem related to teensy bootloader?
		}
		else
		{
			lcdClear();
			lcdGotoXY(0,0);
			lcdPrintData("Nothing Changed",15);
			lcdGotoXY(0,1);
			if (R.USB_data==0)
			lcdPrintData("Serial Data OFF",15);
			else
			lcdPrintData("Serial Data ON",14);
			_delay_ms(200);
			Menu_Mode &=  ~CONFIG;		// We're done
			menu_level = 0;				// We are done with this menu level
			LCD_upd = FALSE;			// Make ready for next time
		}
	}
}


//--------------------------------------------------------------------
// Factory Reset with all default values
//--------------------------------------------------------------------
void factory_menu(void)
{
	static int8_t	current_selection;
	static uint8_t	LCD_upd = FALSE;		// Keep track of LCD update requirements

	// Selection modified by encoder.  We remember last selection, even if exit and re-entry
	if (Status & ENC_CHANGE)
	{
		if (encOutput > 0)
		{
			current_selection++;
		}
		else if (encOutput < 0)
		{
			current_selection--;
		}
  	  	// Reset data from Encoder
		Status &=  ~ENC_CHANGE;
		encOutput = 0;

		// Indicate that an LCD update is needed
		LCD_upd = FALSE;
	}

	// If LCD update is needed
	if (LCD_upd == FALSE)
	{
		LCD_upd = TRUE;					// We have serviced LCD

		// Keep Encoder Selection Within Bounds of the Menu Size
		uint8_t menu_size = factory_menu_size;
		while(current_selection >= menu_size)
			current_selection -= menu_size;
		while(current_selection < 0)
			current_selection += menu_size;

		lcdClear();
		lcdGotoXY(0,0);
		lcdPrintData("All to default?",15);

		// Print the Rotary Encoder scroll Menu
		lcd_scroll_Menu((char**)factory_menu_items, menu_size, current_selection, 1, 0);
	}

	// Enact selection
	if (Status & SHORT_PUSH)
	{
		Status &=  ~SHORT_PUSH;			// Clear pushbutton status

		switch (current_selection)
		{
			case 0:
			lcdClear();
			lcdGotoXY(0,1);
			lcdPrintData("Nothing Changed",15);
			_delay_ms(200);
			Menu_Mode |= CONFIG;// We're NOT done, just backing off
			menu_level = 0;		// We are done with this menu level
			LCD_upd = FALSE;	// Make ready for next time
			break;
			case 1: // Factory Reset
			// Force an EEPROM update:
			R.EEPROM_init_check = 0;		// Initialize eeprom to "factory defaults by indicating a mismatch
			eeprom_write_block(&R.EEPROM_init_check, &E.EEPROM_init_check, sizeof (R.EEPROM_init_check));
			lcdClear();
			lcdGotoXY(0,0);
			lcdPrintData("Factory Reset",13);
			lcdGotoXY(0,1);
			lcdPrintData("All default",11);
			_delay_ms(200);
			asm volatile("jmp 0x00000");	// Soft Reset
			//while (1);					// Bye bye, Death by Watchdog
			//								// BUG, Watchdog is unreliable
			//								// problem related to teensy bootloader?
			default:
			lcdClear();
			lcdGotoXY(0,1);
			lcdPrintData("Nothing Changed",15);
			_delay_ms(200);
			Menu_Mode &=  ~CONFIG;	// We're done
			menu_level = 0;			// We are done with this menu level
			LCD_upd = FALSE;		// Make ready for next time
			break;
		}
	}
}


//
//--------------------------------------------------------------------
// Manage the first level of Menus
//--------------------------------------------------------------------
//
void menu_level0(void)
{
	static int8_t	current_selection;	// Keep track of current LCD menu selection
	static uint8_t	LCD_upd = FALSE;	// Keep track of LCD update requirements

	// Selection modified by encoder.  We remember last selection, even if exit and re-entry
	if (Status & ENC_CHANGE)
	{
		if (encOutput > 0)
		{
			current_selection++;
	    	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		else if (encOutput < 0)
		{
			current_selection--;
	  	  	// Reset data from Encoder
			Status &=  ~ENC_CHANGE;
			encOutput = 0;
		}
		// Indicate that an LCD update is needed
		LCD_upd = FALSE;
	}

	if (LCD_upd == FALSE)				// Need to update LCD
	{
		LCD_upd = TRUE;					// We have serviced LCD

		// Keep Encoder Selection Within Bounds of the Menu Size
		uint8_t menu_size = level0_menu_size;
		while(current_selection >= menu_size)
			current_selection -= menu_size;
		while(current_selection < 0)
			current_selection += menu_size;

		lcdClear();
		lcdGotoXY(0,0);
		lcdPrintData("Config Menu:",12);

		// Print the Menu
		lcd_scroll_Menu((char**)level0_menu_items, menu_size, current_selection,1, 0);
	}

	if (Status & SHORT_PUSH)
	{
		Status &= ~SHORT_PUSH;			// Clear pushbutton status

		switch (current_selection)
		{
			case 0: // PEP sampling period select
				menu_level = PEP_MENU;
				LCD_upd = FALSE;		// force LCD reprint
				break;

			case 1: // Select gain Preset (attenuator is negative)
				menu_level = GAIN_SEL_MENU;
				LCD_upd = FALSE;		// force LCD reprint
				// Jump into the sub-menu indicating the currently selected gain Preset
				gain_selection = R.which_gainset; 
				break;

			case 2: // Adjust Gain compensation
				menu_level = GAIN_MENU;
				LCD_upd = FALSE;		// force LCD reprint
				break;

			case 3: // Calibrate
				menu_level = CAL_MENU;
				LCD_upd = FALSE;		// force LCD reprint
				break;

//			case 3:// Encoder Resolution
//				menu_level = ENCODER_MENU;
//				LCD_upd = FALSE;		// force LCD reprint
//				break;

			case 4: // Serial Data
			menu_level = SERIAL_MENU;
			LCD_upd = FALSE;		// force LCD reprint
			break;

			case 5: // Display Debug stuff
				menu_level = DEBUG_MENU;
				LCD_upd = FALSE;		// force LCD reprint
				break;

			case 6: // Factory Reset
			menu_level = FACTORY_MENU;
			LCD_upd = FALSE;		// force LCD reprint
			break;

			default:
				// Exit
				lcdClear();
				lcdGotoXY(0,1);
		   		lcdPrintData("Return from Menu",16);
				Menu_Mode &=  ~CONFIG;	// We're done
				LCD_upd = FALSE;		// Make ready for next time
		}
	}
}


//
//--------------------------------------------------------------------
// Scan the Configuraton Menu Status and delegate tasks accordingly
//--------------------------------------------------------------------
//
void PushButtonMenu(void)
{
	// Select which menu level to manage
	if (menu_level == 0) menu_level0();

	else if (menu_level == PEP_MENU) pep_menu();

	else if (menu_level == GAIN_SEL_MENU) gain_select_menu();

	else if (menu_level == GAIN_MENU) gain_set_menu();
	else if (menu_level == GAIN_SET1_MENU) gain_menu_level2();
	else if (menu_level == GAIN_SET2_MENU) gain_menu_level2();
	else if (menu_level == GAIN_SET3_MENU) gain_menu_level2();

	else if (menu_level == CAL_MENU) calibrate_menu();
	else if (menu_level == CAL_SET0_MENU) calibrate_menu_level2();
	else if (menu_level == CAL_SET1_MENU) calibrate_menu_level2();
	else if (menu_level == CAL_SET2_MENU) calibrate_menu_level2();

	//else if (menu_level == ENCODER_MENU) encoder_menu();
	
	else if (menu_level == SERIAL_MENU) serial_menu();

	else if (menu_level == DEBUG_MENU) debug_menu();

	else if (menu_level == FACTORY_MENU) factory_menu();
}
